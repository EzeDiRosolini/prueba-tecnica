var app = angular.module('myApp', ['ui.bootstrap']);

app.controller('listadoController', function ($scope, listadoServices) {
    onInit();

    function onInit() {
        $('.loading').show();
        listadoServices.obtnerDatos()
            .then(function (response) {
                console.log(response);
                $scope.listado = response.data.results;
                $scope.pageChanged();
                $('.loading').hide();
            })
            .catch(function (err) {
                console.log(err);
                $('.loading').hide();
            });
    };

    $scope.currentPage = 1;

    $scope.pageChanged = function () {
        $('.loading').show();
        $scope.itemsPerPage = 25;
        $scope.totalItems = $scope.listado.length;

        var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
        var end = begin + $scope.itemsPerPage;

        $scope.filteredItems = $scope.listado.slice(begin, end);

        console.log($scope.filteredItems);
        $('.loading').hide();
    };

    $scope.cargarDetalle = function (_detalle) {
        console.log(_detalle);
        $scope.detalle = _detalle;
    };

    $scope.nombreBuscar = null;

    $scope.buscador = function (_Array) {
        if ($scope.nombreBuscar === null || $scope.nombreBuscar === '') {
            $scope.pageChanged();
        }
        else {
            $scope.filteredItems = _Array.filter(x => x.name.first === $scope.nombreBuscar);
        }
    };
});