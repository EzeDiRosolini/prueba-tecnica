using Entity.Sistema;
using Logic.Sistema;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Models.Sistema;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Site.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UsuarioLogic logic = new UsuarioLogic();
        private readonly IConfiguration _configuration;

        public AccountController(
            IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Login([FromBody] UsuarioModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = logic.GetUsuario(model);

                    if (result != null)
                    {
                        return BuildToken(result);
                    }
                    else
                    {
                        return new JsonResult(new
                        {
                            error = true,
                            mensaje = @"Intento de inicio de sesión no válido."
                        });
                    }
                }
                else
                {
                    return new JsonResult(new
                        {
                            error = true,
                            mensaje = @"Modelo no valido.",
                            ModelState
                        });
                }
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    error = true,
                    mensaje = this.ControllerContext.RouteData.Values["action"] + ": " + ex.Message
                });
            }
        }

        private IActionResult BuildToken(Usuario usuario)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, usuario.Id.ToString()),
                //new Claim("miValor", "Lo que yo quiera"),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["LlaveSecreta"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.Now.AddMinutes(15);

            JwtSecurityToken token = new JwtSecurityToken(
               issuer: "dominio.com",
               audience: "dominio.com",
               claims: claims,
               expires: expiration,
               signingCredentials: creds);

            return new JsonResult(new
            {
                error = false,
                token = new JwtSecurityTokenHandler().WriteToken(token),
                fechaHoraExpiracion = expiration.ToString("dd/MM/yyyy - HH:mm:ss")
            });
        }
    }
}