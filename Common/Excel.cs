using ClosedXML.Excel;
using System;
using System.Data;
using System.IO;

namespace Common
{
    public abstract class Excel
    {
        public Excel()
        {
        }

        public abstract void ModificarHoja(IXLWorksheet ws, int filas);


        public string Generar(DataTable dataTable, string nombreArchivo)
        {
            nombreArchivo = String.Format(@"{0}_{1}.xlsx", nombreArchivo, DateTime.Now.ToString("yyyyMMdd-HHmmss"));

            string rutaPrincipal = AppDomain.CurrentDomain.BaseDirectory + @"\wwwroot\";

            if (!Directory.Exists(rutaPrincipal))
            {
                Directory.CreateDirectory(rutaPrincipal);
            }

            string rutaDownload = String.Format(@"{0}Download\", rutaPrincipal);

            if (!Directory.Exists(rutaDownload))
            {
                Directory.CreateDirectory(rutaDownload);
            }

            string rutaArhivo = String.Format(@"{0}{1}", rutaDownload, nombreArchivo);

            using (XLWorkbook wb = new XLWorkbook())
            {
                IXLWorksheet ws = wb.Worksheets.Add(dataTable, @"Sheet1");

                ModificarHoja(ws, dataTable.Rows.Count);

                wb.CalculateMode = XLCalculateMode.Auto;

                wb.SaveAs(rutaArhivo);
            }

            return String.Format(@"Download/{0}", nombreArchivo);
        }
    }
}
