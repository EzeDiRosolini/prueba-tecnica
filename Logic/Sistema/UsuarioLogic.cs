using Data.Sistema;
using Entity.Sistema;
using Models.Sistema;

namespace Logic.Sistema
{
    public class UsuarioLogic
    {
        public UsuarioLogic()
        {
        }

        public Usuario GetUsuario(UsuarioModel model)
        {
            UsuarioData data = new UsuarioData();

            return data.GetUsuario(model);
        }
    }
}