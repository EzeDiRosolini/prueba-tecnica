using Dapper;
using Entity.Sistema;
using Models.Sistema;
using System.Data.SqlClient;
using System.Linq;

namespace Data.Sistema
{
    public class UsuarioData : SqlServerDataBase
    {
        public UsuarioData()
        {
        }

        public Usuario GetUsuario(UsuarioModel model)
        {
            var resultado = new Usuario();

            using (SqlConnection cnn = GetConexionDataBase())
            {

                string sql = @"SELECT * FROM [dbo].[Usuarios] WHERE CorreoElectronico = @nombreUsuario AND Clave = @clave";

                var queryParameters = new DynamicParameters();
                queryParameters.Add("@nombreUsuario", model.NombreUsuario);
                queryParameters.Add("@clave", model.Clave);

                resultado = cnn.Query<Usuario>(sql, queryParameters).ToList().FirstOrDefault();

            }

            return resultado;
        }
    }
}
